﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace riskLevel
{
    public partial class riskLevelControl: UserControl
    {
        private int value;
        private int maximum;


        public riskLevelControl()
        {
            InitializeComponent();
            this.SetStyle(ControlStyles.UserPaint, true);////使用自定义的绘制方式
            this.Maximum = 3;//设置初始值
        }

        protected override void OnPaint(PaintEventArgs pe)
        {
            if (this.maximum == 2)
            {
                switch (this.value)
                {
                    case 0:
                        this.ForeColor = Color.Green;
                        break;
                    case 1:
                        this.ForeColor = Color.Red;
                        break;
                    default:
                        this.ForeColor = Color.Red;
                        break;
                }
            }
            else if (this.maximum == 3)
            {
                switch (this.value)
                {
                    case 0:
                        this.ForeColor = Color.Green;
                        break;
                    case 1:
                        this.ForeColor = Color.Yellow;
                        break;
                    case 2:
                        this.ForeColor = Color.Red;
                        break;
                    default:
                        this.ForeColor = Color.Red;
                        break;
                }
            }
            else if (this.maximum == 4)
            {
                switch (this.value)
                {
                    case 0:
                        this.ForeColor = Color.Green;
                        break;
                    case 1:
                        this.ForeColor = Color.Violet;
                        break;
                    case 2:
                        this.ForeColor = Color.Yellow;
                        break;
                    case 3:
                        this.ForeColor = Color.Red;
                        break;
                    default:
                        this.ForeColor = Color.Red;
                        break;
                }
            }

            var width = (this.Value + 1) * this.Width / this.Maximum;
            pe.Graphics.FillRectangle(new SolidBrush(this.ForeColor), 0, 0, width, this.Height);
            base.OnPaint(pe);
        }

        [Browsable(true)]
        [Description("风险程度分级,支持2，3，4"), Category("自定义属性")]
        public int Maximum
        {
            get { return maximum; }
            set
            {
                if (value >= 4)
                    this.maximum = 4;
                else
                    this.maximum = value;

                //设置了最大值之后还要检查当前值是否符合要求
                if (this.value >= this.maximum - 1)
                    this.value = this.maximum - 1;

                this.Invalidate();
            }
        }
        [Description("当前风险程度（从0开始，0代表第一级，1代表第二级别.....）,"), Category("自定义属性")]
        public int Value
        {
            get { return value; }
            set
            {
                if (value >= this.maximum - 1)
                    this.value = this.maximum - 1;
                else
                    this.value = value;

                this.Invalidate();
            }
        }
    }
}
